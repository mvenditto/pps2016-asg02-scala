package entities.characters;

import entities.GameEntity;
import entities.objects.GameObject;
import scala.Option;

import java.awt.*;

import static entities.collisions.ContactDirection.AHEAD;
import static entities.collisions.ContactDirection.BACK;

public abstract class BasicEnemy extends BasicCharacter {

    private int deadOffset;

    BasicEnemy(int x, int y, int width, int height, int deadOffset) {
        super(x, y, width, height);
        this.setFacingRight(true);
        this.setMoving(true);
        this.deadOffset = deadOffset;
    }

    public int getDeadOffset() {
        return this.deadOffset;
    }

    @Override
    public void step() {
        if (isAlive()) {
            super.setX(super.x() + (isFacingRight() ? 1 : -1));
        }
    }

    @Override
    public void contactWithObject(GameObject o) {
        if (o.boundingBox().intersects(boundingBox())) {
            o.gotHit(this, Option.empty());
            this.switchDirection(o);
        }
    }

    @Override
    public void contactWithCharacter(BasicCharacter character) {
        this.switchDirection(character);
    }

    private void switchDirection(GameEntity e) {
        if (this.hitAtDirection(e, AHEAD) && this.isFacingRight()) {
            this.setFacingRight(false);
        } else if (this.hitAtDirection(e, BACK) && !this.isFacingRight()) {
            this.setFacingRight(true);
        }
    }

    abstract public Image getDeadImage();
}
