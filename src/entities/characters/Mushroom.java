package entities.characters;

import utils.Res;
import utils.Utils;

import java.awt.*;

public class Mushroom extends BasicEnemy {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT, MUSHROOM_DEAD_OFFSET_Y);
    }


    @Override
    public Image getDeadImage() {
        return Utils.getImage(this.isFacingRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }

    @Override
    public Image getWalkingImage() {
        return Utils.getImage(this.getWalkingImageName(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY));
    }
}
