package entities.characters;

import entities.BasicGameEntity;
import entities.GameEntity;
import entities.collisions.ContactDetection;
import entities.collisions.ContactDirection;
import entities.collisions.ContactInfo;
import utils.Res;

import java.awt.*;

public abstract class BasicCharacter extends BasicGameEntity implements Character {

    protected boolean moving;
    protected boolean facingRight;
    private int counter;
    private boolean alive;

    public BasicCharacter(int x, int y, int width, int height) {
        super(x, y, width, height);
        this.counter = 0;
        this.moving = false;
        this.facingRight = true;
        this.alive = true;
    }

    public abstract void step();

    public abstract Image getWalkingImage();

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setFacingRight(boolean facingRight) {
        this.facingRight = facingRight;
    }

    public boolean isFacingRight() {
        return facingRight;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public boolean isMoving() {return moving;}

    protected String getWalkingImageName(String name, int frequency) {
        return Res.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.facingRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
    }

    public boolean hitAtDirection(GameEntity e, ContactDirection dir) {
        ContactInfo contacts= boundingBox().contacts(e.boundingBox(), ContactDetection::fourDirectionContacts);
        switch (dir) {
            case AHEAD:
                return contacts.ahead();
            case ABOVE:
                return contacts.above();
            case BACK:
                return contacts.back();
            case BELOW:
                return contacts.below();
            default:
                return false;
        }
    }

    public boolean isNearby(GameEntity e) {
        return this.boundingBox().intersects(e.boundingBox());
    }
}
