package entities.characters;

public class CharactersFactory implements AbstractEnemyFactory {

    @Override
    public BasicEnemy createTurtle(int x, int y) {
        return new Turtle(x, y);
    }

    @Override
    public BasicEnemy createMushroom(int x, int y) {
        return new Mushroom(x, y);
    }
}
