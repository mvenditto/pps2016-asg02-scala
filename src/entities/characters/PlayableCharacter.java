package entities.characters;

import java.awt.*;

public interface PlayableCharacter extends Character {

    boolean isJumping();

    void setJumping(boolean jumping);

    Image getJumpImage();

}
