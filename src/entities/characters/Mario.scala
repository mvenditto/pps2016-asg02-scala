package entities.characters

import java.awt.Image

import entities.collisions.{ContactDetection, ContactInfo}
import entities.objects.GameObject
import utils.{Res, Utils}

/**
	* Created by mvenditto.
	*/

trait Mario {
	def getJumpHeightLimit: Int
  def getJumpSpeed: Int
	def getWalkFrequency: Int
	def setSkin(skinName: String): Unit
	def getSkinName: String
}

class MarioImpl(
	  x: Int,
    y: Int,
    w: Int,
    h: Int)
  extends BasicCharacter(x, y, w, h)
  with PlayableCharacter
  with Mario {

	private[this] var jumping: Boolean = false
	private[this] var skin = MarioImpl.DEFAULT_SKIN

	override def step(): Unit = {
		if (moving) {this.setX(_x + (if (isFacingRight) 1 else -1 ))}
		if (jumping) { setY(super.y - getJumpSpeed); jumping = !(super.y <= getJumpHeightLimit)}
		if (!jumping && super.y + getJumpSpeed < MarioImpl.MARIO_OFFSET_Y_INITIAL) {setY(super.y + getJumpSpeed / 2)}
	}

	override def getWalkingImage: Image =
    Utils.getImage(this.getWalkingImageName(skin, getWalkFrequency))

	override def isJumping: Boolean = jumping

	override def setJumping(jumpingFlag: Boolean): Unit = {jumping = jumpingFlag}

	override def getJumpImage: Image = {
		var status: String = ""
		val facing: String = if (this.isFacingRight) {Res.IMGP_DIRECTION_DX} else  {Res.IMGP_DIRECTION_SX}
		status = Res.IMGP_STATUS_SUPER
		Utils.getImage(Res.IMG_BASE + skin + status + facing + Res.IMG_EXT)
	}

	override def contactWithObject(obj: GameObject): Unit = {
		val contacts: ContactInfo = boundingBox contacts(obj.boundingBox, ContactDetection.fourDirectionContacts)

		if (contacts.anyContact) {
			obj.gotHit(this, Option(contacts))
		}

		if (contacts.ahead && this.isFacingRight || contacts.back && !this.isFacingRight) {
			  setMoving(false)
		}

		if (contacts.below) {
				setY(obj.y - height)
		}

		if (contacts.above) {
				setY(obj.y + obj.height)
				jumping = false
		}
	}

	override def contactWithCharacter(other: BasicCharacter): Unit = {
		val contacts: ContactInfo = boundingBox contacts(other.boundingBox, ContactDetection.fourDirectionContacts)
		if (contacts.ahead || contacts.back) {
      if (other.isAlive) {
        setMoving(false)
        setAlive(false)
      } else {
        setAlive(true)
      }
		} else if (contacts.below) {
			other.setMoving(false)
			other.setAlive(false)
		}
	}

	def getWalkFrequency: Int = MarioImpl.DEFAULT_WALK_FREQ

	def getJumpHeightLimit: Int = MarioImpl.DEFAULT_JUMP_LIMIT

  def getJumpSpeed: Int = MarioImpl.DEFAULT_JUMP_SPEED

	def setSkin(skinName: String): Unit = skin = skinName

	def getSkinName: String = skin
}

object MarioImpl {
  val MARIO_WIDTH = 30
  val MARIO_HEIGHT = 50
	val MARIO_OFFSET_Y_INITIAL = 243
	val DEFAULT_SKIN = "mario"
	val FIRE_SKIN = "mario_fire"
	val DEFAULT_JUMP_SPEED = 4
	val DEFAULT_JUMP_LIMIT = 50
	val DEFAULT_WALK_FREQ = 25

  def apply(x: Int, y: Int): MarioImpl = new MarioImpl(x, y, MARIO_WIDTH, MARIO_HEIGHT)
}
