package entities.characters;


public interface AbstractEnemyFactory {

    BasicEnemy createTurtle(int x, int y);

    BasicEnemy createMushroom(int x, int y);

}
