package entities.collisions;


public enum ContactDirection {
    ABOVE,
    BELOW,
    AHEAD,
    BACK
}
