package entities.collisions

/**
  * Created by vendo.
  */

case class ContactInfo(ahead: Boolean, back: Boolean, above: Boolean, below: Boolean) {
  def anyContact: Boolean = ahead || back || above || below
}

sealed trait Bounds {
  def minX: Int

  def maxX: Int

  def minY: Int

  def maxY: Int

  def centerX: Int

  def centerY: Int

  def contains(x: Int, y: Int): Boolean

  def intersects(other: Bounds): Boolean
}

class BoundingBox(val topLeft: (Int, Int), val dim: (Int, Int)) extends Bounds {

  def minX: Int = topLeft._1

  def maxX: Int = minX + dim._1

  def minY: Int = topLeft._2

  def maxY: Int = minY + dim._2

  def centerX: Int = minX + ((maxX - minX) / 2)

  def centerY: Int = minY + ((maxY - minY) / 2)

  def contains(x: Int, y: Int): Boolean =
    x >= minX && x <= maxX && y >= minY && y <= maxY

  def intersects(other: Bounds): Boolean =
    minX < other.maxX && maxX > other.minX && minY < other.maxY && maxY > other.minY

  def resize(dim: (Int, Int)): BoundingBox = BoundingBox((minX, minY), dim)

  def move(pos: (Int, Int)): BoundingBox = BoundingBox(pos, dim)

  def contacts(other: BoundingBox,
    strategy: (BoundingBox, BoundingBox) => ContactInfo): ContactInfo = strategy(this, other)

  override def toString: String = "(" + minX + " " + minY + " " + maxX + " " + maxY + ")"

}

object BoundingBox {
  def apply(topLeft: (Int, Int),
    bottomRight: (Int, Int)): BoundingBox = new BoundingBox(topLeft, bottomRight)
}

object ContactDetection {
  private[this] val contactRadius: Int = 20

  def fourDirectionContacts(a: BoundingBox, b: BoundingBox): ContactInfo = {
    val ahead = b contains(a.maxX + contactRadius, a.centerY)
    val back = b contains(a.minX - contactRadius, a.centerY)
    val above = b contains(a.centerX, a.minY - contactRadius)
    val below = b contains(a.centerX, a.maxY + contactRadius)
    ContactInfo(ahead, back, above, below)
  }
}