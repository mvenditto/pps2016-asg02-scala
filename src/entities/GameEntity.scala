package entities

import entities.collisions.BoundingBox

/**
  * Created by mvenditto.
  */

trait GameEntity {
  def x: Int

  def y: Int

  def width: Int

  def height: Int

  def setX(newX: Int): Unit

  def setY(newY: Int): Unit

  def setWidth(newWidth: Int): Unit

  def setHeight(newHeight: Int): Unit

  def boundingBox: BoundingBox
}

class BasicGameEntity(
    var _x: Int,
    var _y: Int,
    var _w: Int,
    var _h: Int) extends GameEntity {

  private[this] var bbox: BoundingBox = BoundingBox((_x, _y), (_w, _h))

  def x: Int = _x

  def y: Int = _y

  def width: Int = _w

  def height: Int = _h

  def setX(newX: Int): Unit = {_x = newX; bbox = bbox.move((_x, _y))}

  def setY(newY: Int): Unit = {_y = newY; bbox = bbox.move((_x, _y))}

  def setWidth(newWidth: Int): Unit = {_w = newWidth; bbox = bbox.resize((newWidth, height))}

  def setHeight(newHeight: Int): Unit = {_h = newHeight; bbox = bbox.resize((width, newHeight))}

  def boundingBox: BoundingBox = bbox
}