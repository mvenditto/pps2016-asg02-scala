package entities.objects

import java.awt.Image

import entities.characters.{BasicEnemy, Mario, MarioImpl}
import entities.collisions.ContactInfo
import entities.{BasicGameEntity, GameEntity}
import utils.{Res, Utils}

/**
  * Created by mvenditto.
  */

sealed trait GameObject extends GameEntity {
  def step(): Unit

  def getObjectImage: Image

  def gotHit(ent: GameEntity, contact: Option[ContactInfo]): Unit

  def dead: Boolean
}

abstract class BasicGameObject(
    val pos: (Int, Int),
    val dim: (Int, Int),
    val imgPath: String)
  extends BasicGameEntity(pos._1, pos._2, dim._1, dim._2) with GameObject {

  private[this] val objectImage: Image = Utils.getImage(imgPath)
  protected[this] var _dead = false

  def getObjectImage: Image = objectImage

  def dead: Boolean = _dead

  def step(): Unit = ()

  def gotHit(ent: GameEntity, contact: Option[ContactInfo]): Unit = ()
}

object BasicGameObject {
  def createTunnel(pos: (Int, Int)): BasicGameObject = Tunnel(pos)

  def createBlock(pos: (Int, Int)): BasicGameObject = Block(pos)

  def createPiece(pos: (Int, Int)): BasicGameObject = Piece(pos)

  def creteMushroomPowerUp(pos: (Int, Int)): BasicGameObject = MushroomPowerUp(pos)
}

private object ObjectSize {
  val BASIC_BLOCK: (Int, Int) = (30, 30)
  val BASIC_TUNNEL: (Int, Int) = (43, 65)
  val BASIC_COIN: (Int, Int) = (30, 30)
  val BASIC_MUSHROOM_PU: (Int, Int) = (30, 30)
  val FIREBALL: (Int, Int) = (11, 11)
}

case class Tunnel(override val pos: (Int, Int))
  extends BasicGameObject(pos, ObjectSize.BASIC_TUNNEL, Res.IMG_TUNNEL)

case class Block(override val pos: (Int, Int))
  extends BasicGameObject(pos, ObjectSize.BASIC_BLOCK, Res.IMG_BLOCK)

case class Piece(override val pos: (Int, Int))
  extends BasicGameObject(pos, ObjectSize.BASIC_COIN, Res.IMG_PIECE1)

case class MushroomPowerUp(override val pos: (Int, Int))
  extends BasicGameObject(pos, ObjectSize.BASIC_MUSHROOM_PU, Res.IMG_BIG_MARIO_POWER_UP) {

  override def gotHit(ent: GameEntity, contact: Option[ContactInfo]): Unit = {
    ent match {
      case m: Mario => m.setSkin(MarioImpl.FIRE_SKIN); _dead = true
      case _ => ()
    }
  }
}

case class Fireball(override val pos: (Int, Int))
    extends BasicGameObject(pos, ObjectSize.FIREBALL, Res.IMG_FIREBALL) {

  private[this] val vx = 2
  private[this] val vy = 0
  private[this] var life = 500

  override def step(): Unit = setX(super.x + vx); setY(super.y + vy); life -= 1; _dead = life <= 0

  override def gotHit(ent: GameEntity, contact: Option[ContactInfo]): Unit = {
    ent match {
      case e: BasicEnemy => e.setAlive(false)
      case _: GameObject => _dead = true
      case _ => ()
    }
  }
}