package test

import entities.collisions.{BoundingBox, ContactDetection}
import org.junit.Assert._
import org.junit.{Before, Test}

class BoundingBoxTest {

    private[this] var boundingBox: BoundingBox = _

    private[this] val MIN_X = 0
    private[this] val WIDTH = 20
    private[this] val MIN_Y = 0
    private[this] val HEIGHT = 10
    private[this] val PROXIMITY_RANGE = 10
    private[this] val HALF_PROXIMITY_RANGE = PROXIMITY_RANGE / 2

    private def createDefaultDimBBox(pos: (Int, Int)): BoundingBox = BoundingBox(pos, (WIDTH, HEIGHT))

    @Before def setUp(): Unit = {
        boundingBox = createDefaultDimBBox((MIN_X, MIN_Y))
    }

    @Test def testShouldNotIntersect() {
        val bbox = createDefaultDimBBox((MIN_X + WIDTH * 2, MIN_Y + HEIGHT * 2))
        assertFalse (boundingBox intersects bbox)
    }

    @Test def testShouldIntersect(){
        val bbox = createDefaultDimBBox(MIN_X + WIDTH / 2, MIN_Y + HEIGHT / 2)
        assertTrue (boundingBox intersects bbox)
    }

    @Test def testShouldContactAhead(){
       val aheadBox = createDefaultDimBBox((MIN_X + WIDTH + HALF_PROXIMITY_RANGE, MIN_Y))
        assertTrue(boundingBox.contacts(aheadBox, ContactDetection.fourDirectionContacts).ahead)
    }

    @Test def testShouldContactBack(){
        val backBox = createDefaultDimBBox((MIN_X - WIDTH - HALF_PROXIMITY_RANGE, MIN_Y))
        assertTrue ((boundingBox contacts (backBox, ContactDetection.fourDirectionContacts)).back)
    }

    @Test def testShouldContactAbove(){
        val aboveBox = createDefaultDimBBox((MIN_X, MIN_Y - HEIGHT- HALF_PROXIMITY_RANGE))
        assertTrue ((boundingBox contacts (aboveBox, ContactDetection.fourDirectionContacts)).above)
    }

    @Test def testShouldContactBelow(){
        val belowBox = createDefaultDimBBox((MIN_X, MIN_Y + HEIGHT + HALF_PROXIMITY_RANGE))
        assertTrue ((boundingBox contacts (belowBox, ContactDetection.fourDirectionContacts)).below)
    }

    @Test def testShouldContainPoint(){
        assertTrue(boundingBox.contains(boundingBox.centerX, boundingBox.centerY))
    }
}