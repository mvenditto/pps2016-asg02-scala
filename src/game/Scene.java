package game;

import utils.Res;
import utils.Utils;

import javax.swing.*;
import java.awt.*;

public class Scene extends JPanel {

    public static final int BACKGROUND_OFFSET_X = -50;
    public static final int BACKGROUND_OFFSET_Y = 0;
    public static final int MARIO_OFFSET_X = 300;
    public static final int MARIO_OFFSET_Y = 245;
    private Image imgBackground;
    private Image imgMario;

    public Scene() {
        super();
        this.imgBackground = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        g2.drawImage(this.imgBackground, BACKGROUND_OFFSET_X, BACKGROUND_OFFSET_Y, null);
        g2.drawImage(imgMario, MARIO_OFFSET_X, MARIO_OFFSET_Y, null);
    }
}
