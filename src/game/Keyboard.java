package game;

import entities.GameEntity;
import entities.characters.Mario;
import entities.characters.MarioImpl;
import entities.objects.Fireball;
import scala.Tuple2;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Game.getScene().getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                Game.getScene().getMario().setFacingRight(true);
                Game.getScene().getMario().setMoving(true);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                Game.getScene().getMario().setMoving(true);
                Game.getScene().getMario().setFacingRight(false);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Game.getScene().getMario().setJumping(true);
                //Audio.playSound("/resources/audio/jump.wav");
            }

            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                GameEntity m = Game.getScene().getMario();
                if (((Mario)m).getSkinName().equals(MarioImpl.FIRE_SKIN())) {
                    Game.getScene().spawnGameObject(new Fireball(new Tuple2<>(m.x() + m.width() + 10, m.y() + m.height() / 2)));
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Game.getScene().getMario().setMoving(false);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
