package game;

public class Refresh implements Runnable {

    private static int PAUSE = 4;

    public void run() {
        while (true) {
            Game.getScene().repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (Exception ignored) {

            }
        }
    }

} 
