package game

import java.awt.Graphics
import java.io.InputStream
import javax.swing.JPanel

import entities.characters._
import entities.collisions.BoundingBox
import entities.objects.{BasicGameObject, GameObject}
import utils.{Res, Utils}

import scala.io.Source

/**
  * Created by mvenditto.
  */

class Platform extends JPanel {

  val charactersFactory = new CharactersFactory

  private val imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND)
  private val imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND)
  private val castle = Utils.getImage(Res.IMG_CASTLE)
  private val start = Utils.getImage(Res.START_ICON)
  private val mario = MarioImpl(300, 245)
  private val imgFlag = Utils.getImage(Res.IMG_FLAG)
  private val imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL)

  private val FLAG_X_POS = 4650
  private val CASTLE_X_POS = 4850
  private val FLAG_Y_POS = 115
  private val CASTLE_Y_POS = 145
  private val CASTLE_START_X = 10
  private val CASTLE_START_Y = 95
  private val START_X = 220
  private val START_Y = 234

  private val background1PosX = -50
  private var collectedCoins = 0

  private var objects = Seq.empty[GameObject]
  private var pieces = Seq.empty[GameObject]
  private var enemies = Seq.empty[BasicEnemy]

  private var debugDraw = false

  def loadMapFromTxt(filePath: String): Unit = {
    val stream : InputStream = getClass.getResourceAsStream(filePath)
    for (line <- Source.fromInputStream(stream).getLines()) {
      (line split ":" ).toList match {
        case "Block" :: x :: y :: _ => objects :+= BasicGameObject.createBlock((x.toInt, y.toInt))
        case "Turtle" :: x :: y :: _ => enemies :+= new CharactersFactory().createTurtle(x.toInt, y.toInt)
        case "Mushroom" :: x :: y :: _ => enemies :+= new CharactersFactory().createMushroom(x.toInt, y.toInt)
        case "Tunnel" :: x :: y :: _ => objects :+= BasicGameObject.createTunnel((x.toInt, y.toInt))
        case "Piece" :: x :: y :: _ => pieces :+= BasicGameObject.createPiece((x.toInt, y.toInt))
        case "MushroomPowerUp" :: x :: y :: _ => objects :+= BasicGameObject.creteMushroomPowerUp((x.toInt, y.toInt))
        case _ => ()
      }
    }
  }

  private def worldToScreen(x: Int): Int = x - (mario.x - getWidth / 2)

  def setDebugDraw(flag: Boolean): Unit = debugDraw = flag

  def getMario: MarioImpl = mario

  def spawnGameObject(o: GameObject): Unit = objects :+= o

  private def stepWorld() {
    mario.step()
    enemies.foreach((e: BasicEnemy) => e.step())
    objects.foreach((o: GameObject) => o.step())
    removeDeadObjects()
  }

  private def solveCollisions() {
    var hitSomething: Boolean = false

    objects.foreach((o: GameObject) => {
      if (mario.isNearby(o)) {
        hitSomething = true
        mario.contactWithObject(o)
      }

      enemies.foreach((e: BasicEnemy) => {
        objects.foreach((o2: GameObject) => e.contactWithObject(o2))
      })
    })

    var toRemove = Seq.empty[GameObject]
    pieces.foreach((p: GameObject) => {
      if (mario.isNearby(p)) {
        Audio.playSound(Res.AUDIO_MONEY)
        toRemove :+= p
        collectedCoins += 1
      }
    })
    pieces = pieces.filter(!toRemove.contains(_))

    enemies.foreach((e: BasicEnemy) => {
      enemies.foreach((e2: BasicEnemy) => {
        if (e.isNearby(e2)) {
          e.contactWithCharacter(e2)
        }
      })
      if (mario.isNearby(e)) {
        mario.contactWithCharacter(e)
      }
    })
  }

  private def removeDeadObjects(): Unit = {
    var toRemove = Seq.empty[GameObject]
    objects.foreach((o: GameObject) => if (o.dead) toRemove :+= o)
    objects = objects.filter(!toRemove.contains(_))
  }

  private def drawGameObjects(g2: Graphics, objectsToDraw: Seq[GameObject]) {
    objectsToDraw.foreach((o: GameObject) => {
      g2.drawImage(o.getObjectImage, worldToScreen(o.x), o.y, null)
      if (debugDraw) {
        val bbox: BoundingBox = o.boundingBox
        g2.drawRect(worldToScreen(bbox.minX), bbox.minY, bbox.maxX - bbox.minX, bbox.maxY - bbox.minY)
      }
    })
  }

  private def drawEnemies(g2: Graphics) {
    enemies.foreach((e: BasicEnemy) => {
      if (e.isAlive) {
        g2.drawImage(e.getWalkingImage, worldToScreen(e.x), e.y, null)
      } else {
        g2.drawImage(e.getDeadImage, worldToScreen(e.x), e.y + e.getDeadOffset, null)
      }
    })
  }

  private def drawBackground(g2: Graphics) {
    g2.drawImage(imgBackground2, background1PosX, 0, null)
    g2.drawImage(imgBackground1, background1PosX, 0, null)
    g2.drawImage(castle, worldToScreen(CASTLE_START_X), CASTLE_START_Y, null)
    g2.drawImage(start,worldToScreen(START_X), START_Y, null)
  }

  private def drawCastleAndFlag(g2: Graphics) {
    g2.drawImage(imgFlag, worldToScreen(FLAG_X_POS), FLAG_Y_POS, null)
    g2.drawImage(imgCastle, worldToScreen(CASTLE_X_POS), CASTLE_Y_POS, null)
  }

  private def drawMario(g2: Graphics) {
    if (mario.isJumping) {
      g2.drawImage(mario.getJumpImage, worldToScreen(mario.x), mario.y, null)
    } else {
      g2.drawImage(mario.getWalkingImage, worldToScreen(mario.x), mario.y, null)
    }
  }

  private def drawCollectedCoins(g2: Graphics) {
    g2.drawImage(Utils.getImage(Res.IMG_PIECE1), 10, 10, null)
    g2.drawString(Integer.toString(collectedCoins), 40, 30)
  }

  private def debugDraw(g2: Graphics) {
    val bbox: BoundingBox = mario.boundingBox
    g2.drawRect(worldToScreen(bbox.minX), bbox.minY, bbox.maxX - bbox.minX, bbox.maxY - bbox.minY)
    g2.fillOval(worldToScreen(bbox.maxX + 10), bbox.centerY, 3, 3)
    g2.fillOval(worldToScreen(bbox.minX - 10), bbox.centerY, 3, 3)
    g2.fillOval(worldToScreen(bbox.centerX), bbox.maxY + 10, 3, 3)
    g2.fillOval(worldToScreen(bbox.centerX), bbox.minY - 10, 3, 3)
    enemies.foreach((e: BasicEnemy) => {
      val enemyBoundingBox: BoundingBox = e.boundingBox
      g2.drawRect(worldToScreen(enemyBoundingBox.minX), enemyBoundingBox.minY, e.width, e.height)
    })
  }

  override def paintComponent(g2: Graphics)  {
    super.paintComponent(g2)
    solveCollisions()
    stepWorld()
    drawBackground(g2)
    drawGameObjects(g2, objects)
    drawGameObjects(g2, pieces)
    drawCastleAndFlag(g2)
    drawMario(g2)
    drawEnemies(g2)
    drawCollectedCoins(g2)
    if (debugDraw) {
      debugDraw(g2)
    }
  }
}
