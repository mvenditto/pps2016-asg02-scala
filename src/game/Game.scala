package game

import javax.swing.JFrame

import utils.Res

/**
  * Created by mvenditto.
  */
object Game {
  private[this] val WINDOW_WIDTH = 700
  private[this] val WINDOW_HEIGHT = 360
  private[this] val WINDOW_TITLE = "Super Mario"
  private[this] val window = new JFrame(WINDOW_TITLE)
  private[this] val scene = new Platform
  private[this] val timer = new Thread(new Refresh)

  def getScene: Platform = scene

  def main(args: Array[String]): Unit = {
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT)
    window.setLocationRelativeTo(null)
    window.setResizable(true)
    window.setAlwaysOnTop(true)
    scene.setDebugDraw(false)
    window.setContentPane(scene)
    scene.setFocusable(true)
    scene.requestFocusInWindow()
    scene.addKeyListener(new Keyboard())
    scene.loadMapFromTxt(Res.LEVEL_BASE + Res.STAGE_1)
    window.setVisible(true)
    println(scene)
    timer.start()
  }
}
