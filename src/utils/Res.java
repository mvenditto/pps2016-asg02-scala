package utils;

/**
 * @author Roberto Casadei
 */

public class Res {
    public static final String IMG_BASE = "/resources/imgs/";
    public static final String AUDIO_BASE = "/resources/audio/";
    public static final String LEVEL_BASE = "/resources/levels/";
    public static final String IMG_EXT = ".png";

    public static final String STAGE_1 = "stage1";

    public static final String IMGP_STATUS_NORMAL = "";
    public static final String IMGP_STATUS_ACTIVE = "A";
    public static final String IMGP_STATUS_DEAD = "E";
    public static final String IMGP_STATUS_IDLE = "F";
    public static final String IMGP_STATUS_SUPER = "S";

    public static final String IMGP_DIRECTION_SX = "G";
    public static final String IMGP_DIRECTION_DX = "D";

    public static final String IMGP_CHARACTER_MUSHROOM = "mushroom";
    public static final String IMGP_CHARACTER_TURTLE = "turtle";
    public static final String IMGP_CHARACTER_MARIO = "mario";
    public static final String IMGP_CHARACTER_MARIO_FIRE = "mario_fire";

    public static final String IMGP_OBJECT_BLOCK = "block";
    public static final String IMGP_OBJECT_PIECE1 = "piece1";
    public static final String IMGP_OBJECT_PIECE2 = "piece2";
    private static final String IMGP_OBJECT_TUNNEL = "tunello";

    public static final String IMG_FIREBALL = IMG_BASE + "fireball" + IMG_EXT;

    public static final String IMG_MARIO_DEFAULT_FIRE = IMG_BASE + IMGP_CHARACTER_MARIO_FIRE + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_SX_FIRE = IMG_BASE + IMGP_CHARACTER_MARIO_FIRE + IMGP_STATUS_SUPER + IMGP_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_DX_FIRE = IMG_BASE + IMGP_CHARACTER_MARIO_FIRE + IMGP_STATUS_SUPER + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_SX_FIRE = IMG_BASE + IMGP_CHARACTER_MARIO_FIRE + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_DX_FIRE = IMG_BASE + IMGP_CHARACTER_MARIO_FIRE + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SX_FIRE = IMG_BASE + IMGP_CHARACTER_MARIO_FIRE + IMGP_STATUS_NORMAL + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_DX_FIRE = IMG_BASE + IMGP_CHARACTER_MARIO_FIRE + IMGP_STATUS_NORMAL + IMGP_DIRECTION_DX + IMG_EXT;


    public static final String IMG_MARIO_DEFAULT = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_SX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_SUPER + IMGP_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_DX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_SUPER + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_SX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_DX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_NORMAL + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_DX = IMG_BASE + IMGP_CHARACTER_MARIO + IMGP_STATUS_NORMAL + IMGP_DIRECTION_DX + IMG_EXT;

    public static final String IMG_MUSHROOM_DX = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_NORMAL + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MUSHROOM_SX = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_NORMAL + IMGP_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEAD_DX = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_DEAD + IMGP_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEAD_SX = IMG_BASE + IMGP_CHARACTER_MUSHROOM + IMGP_STATUS_DEAD + IMGP_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEFAULT = Res.IMG_BASE + Res.IMGP_CHARACTER_MUSHROOM + Res.IMGP_STATUS_ACTIVE + IMGP_DIRECTION_DX + IMG_EXT;

    public static final String IMG_TURTLE_IDLE = IMG_BASE + IMGP_CHARACTER_TURTLE + IMGP_STATUS_IDLE + IMG_EXT;
    public static final String IMG_TURTLE_DEAD = IMG_TURTLE_IDLE;

    public static final String IMG_BLOCK = IMG_BASE + IMGP_OBJECT_BLOCK + IMG_EXT;

    public static final String IMG_PIECE1 = IMG_BASE + IMGP_OBJECT_PIECE1 + IMG_EXT;
    public static final String IMG_PIECE2 = IMG_BASE + IMGP_OBJECT_PIECE2 + IMG_EXT;
    public static final String IMG_TUNNEL = IMG_BASE + IMGP_OBJECT_TUNNEL + IMG_EXT;

    public static final String IMG_BACKGROUND = IMG_BASE + "background" + IMG_EXT;
    public static final String IMG_CASTLE = IMG_BASE + "castelloIni" + IMG_EXT;
    public static final String START_ICON = IMG_BASE + "start" + IMG_EXT;
    public static final String IMG_CASTLE_FINAL = IMG_BASE + "castelloF" + IMG_EXT;
    public static final String IMG_FLAG = IMG_BASE + "bandiera" + IMG_EXT;

    public static final String IMG_BIG_MARIO_POWER_UP = IMG_BASE + "mushroom_big_mario" + IMG_EXT;

    public static final String AUDIO_MONEY = AUDIO_BASE + "money.wav";
}
